class Machine {
    constructor() {
        this.enabled = false;
    }
    turnOn() {
        if (!this.plugged) {
            console.log('You can not turn on the plugged off item!');
        } else {
            this.enabled = true;
            console.log(`${this.name} is turned on!`);
        }
    }
    turnOff() {
        this.enabled = false;
        console.log(`${this.name} is turned off!`);
    }
}

class HomeAppliance extends Machine {
    constructor() {
        super();
        this.plugged = false;
    }
    plugIn() {
        this.plugged = true;
        console.log('plugged in!');
    }
    plugOff() {
        this.plugged = false;
        console.log('plugged off!');
    }
}

class WashingMachine extends HomeAppliance {
    constructor(name = 'Washing Machine') {
        super();
        this.name = name;
    }
}

class LightSource extends HomeAppliance {
    constructor(name = 'Light Source') {
        super();
        this.name = name;
        this.lightLevel = 1;
    }
    setLevel(userLevel) {
        if (userLevel >= 1 && userLevel <= 100) {
            this.lightLevel = userLevel;
        }
        console.log(this.lightLevel + ' light level is set.');
    }
}

class AutoVechicle extends Machine {
    constructor() {
        super();
        this.x = 0;
        this.y = 0;
    }
    setPosition(xPoint, yPoint) {
        this.x = xPoint;
        this.y = yPoint;
    }
    turnOn() {
        console.log(`${this.name} is turned on!`);

    }
}

class Car extends AutoVechicle {
    constructor(name = 'Car') {
        super();
        this.name = name;
        this.speed = 10;
    }
    setSpeed(userSpeed) {
        this.speed = userSpeed;
        console.log(`${this.name}'s speed is ${this.speed}`);
    }
    run(x, y) {
        console.log('Running!');
        let move = setInterval(() => {
            if (this.x < x || this.y < y) {
                this.setPosition(this.x + this.speed, this.y + this.speed);
                if (this.x > x) {
                    this.setPosition(x, this.y);
                }

                if (this.y > y) {
                    this.setPosition(this.x, y);
                }

                console.log(this.x + ', ' + this.y);
            } else {
                clearInterval(move);
                console.log('You are in the right place now.')
            }
        }, 1000);
    }
}

var bosch = new WashingMachine();
bosch.name = 'Bosch';
bosch.turnOn();
bosch.plugIn();
bosch.turnOn();
console.log('-------------');

var lightBulb = new LightSource('Light bulb');
lightBulb.plugIn();
lightBulb.setLevel(60);
lightBulb.turnOn();
console.log('-------------');

var honda = new Car('Honda');
honda.setPosition(30, 40);
honda.turnOn();
honda.setSpeed(60);
honda.run(180, 240);

